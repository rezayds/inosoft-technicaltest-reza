<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
// use use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Kendaraan extends Model
{
    use HasFactory;
    // protected $connection = 'mongodb';
    // protected $collection = 'kendaraans';

    protected $guarded = ['id'];
}
